import './App.css'

const cards = [
  {
    title: 'Tips para viajar con poco presupuesto',
    publicado: '19 Julio, 2018',
    por: 'Blog Viajes',
    img: 'img/01.jpg'
  },
  {
    title: 'Todo lo necesario para tu viaje',
    publicado: '19 Julio, 2018',
    por: 'Blog Viajes',
    img: 'img/02.jpg'
  },
  {
    title: 'El equipo necesario para tomar las mejores fotografías',
    publicado: '19 Julio, 2018',
    por: 'Blog Viajes',
    img: 'img/03.jpg'
  },
  {
    title: 'Checklist para tu próximo viaje',
    publicado: '19 Julio, 2018',
    por: 'Blog Viajes',
    img: 'img/04.jpg'
  },
  {
    title: 'Los mejores lugares para visitar este otoño',
    publicado: '19 Julio, 2018',
    por: 'Blog Viajes',
    img: 'img/05.jpg'
  },
  {
    title: 'Los mejores lugares para visitar con montañas',
    publicado: '19 Julio, 2018',
    por: 'Blog Viajes',
    img: 'img/06.jpg'
  }
]


function App() {
  return (
    <div className='max-w-screen-lg my-0 mx-auto' >
      <main className="sm:flex sm:flex-wrap sm:justify-between">
        <h2>Últimas Entradas</h2>
        {cards.map(card => (
          <article className="entrada mb-4">
            <img className='max-w-full' src={card.img} alt="texto entrada" />
            <div className='bg-white text-center p-4 uppercase'>
              <h3 className='text-sm leading-6 '>{card.title}</h3>
              <p>Publicado el: <span>{card.publicado}</span></p>
              <p>Por: <span>{card.por}</span></p>
              <a href="#!" className="bg-blue-600 text-white px-12 py-2 mt-4 inline-block no-underline"> Leer más</a>
            </div>
          </article>
        ))}
      </main>
    </div>
  );
}

export default App;
